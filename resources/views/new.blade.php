<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <script>
        window.Laravel = {
            csrfToken: '{{csrf_token()}}'
        }
    </script>
    <title>Straight Media (EA)</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="css/app.css" media="screen,projection" />
    <!-- Compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <div class="carousel carousel-slider">
        <a class="carousel-item" href="#one!">
            <img src="https://picsum.photos/800/350">
        </a>
        <a class="carousel-item" href="#two!">
            <img src="https://picsum.photos/800/350">
        </a>
        <a class="carousel-item" href="#three!">
            <img src="https://picsum.photos/800/350">
        </a>
        <a class="carousel-item" href="#four!">
            <img src="https://picsum.photos/800/350">
        </a>
    </div>
    {{--
    <div class="intro" name="app">
        <div class="container center">
            <div class="row center white-text">
                <div class="col s12">
                    <h1>This pen is
                        <span class="txt-rotate" data-period="2000" data-rotate='[ "nerdy.", "simple.", "pure JS.", "pretty.", "fun!" ]'></span>
                    </h1>
                    <h2>A single &lt;span&gt; is all you need.</h2>
                </div>
            </div>
        </div>
    </div> --}}
    <nav class="teal">
        <div class="nav-wrapper">
            <div class="container">
                <a href="#!" class="brand-logo">Logo</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger">
                    <i class="material-icons">menu</i>
                </a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                    <li>
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li>
                        <a href="contact">Contacts</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <ul class="sidenav" id="mobile-demo">
        <div class="card" style="padding: 0px; box-shadow: 5px">
            <div class="card-image">
                <img src="https://picsum.photos/500/400">
                <span class="card-title center">Branding</span>
            </div>
        </div>

        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#about">About</a>
        </li>
        <li>
            <a href="#services">Services</a>
        </li>
        <li>
            <a href="#portfolio">Portfolio</a>
        </li>
        <li>
            <a href="contact">Contacts</a>
        </li>
    </ul>
    {{-- lorem*100 --}}
    <section id="about" style="padding-top: 50px">
        <div>
            <div class="container">
                <div class="row">
                    <div class="col s12 center">
                        <div class="grey-text text-darken-3">
                            <h2>About us</h2>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <span class="flow-text">
                            Straight Media E.A Ltd — is a modern Sign Manufacturing Company established with the intention of providing high quality
                            cost effective signage on time. We have established ourselves as being commited in bringing quality
                            service & meeting our clients' deadline in a short period of time. Our Company is capable of
                            handling branding projects from small to large multi-national companies. We are the preferred
                            supplier for various signs for: -Seneca, Saab Royale Hotel, Suraya Properties, Co-operative,
                            TRV Kenya, Yala Enterprises amongst others. We are currently involved in supplying signage and
                            branding for: • Kentmere Valley • Muthaiga Heights • Karen Office Park • Nyoro Road Contactors
                            We also provide promotional material like t-shirts, calendars, Brochures & we also provide graphic
                            designing services. Each job is a customized job, and therefore special attention is paid to
                            every minute detail in the sign. Straight Media EA Ltd is able to control and ensure quality
                            service delivery at each stage of production of all Sign items, are manufactured and assembled
                            locally at our factory, on Industrial Area, Nairobi, Kenya.
                        </span>
                    </div>
                    <div class="col s12 m6 l6">
                        <span class="flow-text">
                            One of our important measurements of performance is of repeat business and not replacement orders from our corporate customers.
                            This gives us an excellent opportunity to evaluate our product and service quality. Additional
                            orders indicate customer satisfaction. Feedback from our customers plays a very important role
                            in ensuring that products meets customers expectations. As a Sign supplier located in East Africa
                            we have several advantages that we would like to bring to your attention. The signs would attract
                            low rates of Customs Duty due to the fact that we are based in a member country of COMESA. All
                            the countries listed as members of a common Market COMESA will attract low duties compared to
                            signs from non member countries. Freight Costs would be lower, as items could be sent through
                            surface routesor by sea though urgent items can be shipped by air. We also undertake painting
                            contracts that includes scraping, scheming, sanding and painting of new buldings at competitive
                            rate using the preffered piants by our clients.
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--
    <div class="divider"></div> --}}
    <div class="parallax-container">
        <div class="parallax">
            <img src="https://picsum.photos/1920/1080/">
        </div>
    </div>
    <section id="services">
        <div class="container">
            <div class="row center">
                <div class="col s12">
                    <span class="text-grey text-darken-4">
                        <h2>
                            Services
                        </h2>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m4">
                    <div class="card hoverable" style="padding: 0px; box-shadow: 5px">
                        <div class="card-image">
                            <img src="https://picsum.photos/500/400">
                            <span class="card-title center">Branding</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card hoverable" style="padding: 5px">
                        <div class="card-image">
                            <img src="https://picsum.photos/500/300">
                            <span class="card-title center">Branding</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card hoverable" style="padding: 5px">
                        <div class="card-image">
                            <img src="https://picsum.photos/500/300">
                            <span class="card-title center">Branding</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card hoverable" style="padding: 5px">
                        <div class="card-image">
                            <img src="https://picsum.photos/500/300">
                            <span class="card-title center">Branding</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{--
    <div class="divider"></div> --}}
    <div class="parallax-container">
        <div class="parallax">
            <img src="https://picsum.photos/1920/1080/">
        </div>
    </div>
    <section id="culture">
        <div class="container">
            <div class="row">
                <div class="col s12 center">
                    <span>
                        <h2>Our culture</h2>
                    </span>
                </div>
            </div>
            <div class="row"></div>
        </div>
    </section>
    {{--
    <div class="divider"></div> --}}
    <div class="parallax-container">
        <div class="parallax">
            <img src="https://picsum.photos/1920/1080/">
        </div>
    </div>
    <section id="people">
        <div class="container">
            <div class="row center">
                <div class="col sm12 l6">
                    <h2>People</h2>

                </div>
            </div>
        </div>
    </section>
    {{--
    <div class="divider"></div> --}}
    <div class="parallax-container">
        <div class="parallax">
            <img src="https://picsum.photos/1920/1080/">
        </div>
    </div>
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col s12 center">
                    <span>
                        <h2>Portfolio</h2>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col s12 center">
                    <span class="title">Here are some of the things we've done</span>


                </div>
            </div>

            <div class="row">
                <div class="col s12 m12 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">
                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4">
                    <div class="card hoverable">
                        <div class="card-image">

                            <img class="materialboxed" src="https://picsum.photos/768/768.jpg" alt="/images/loading.jpg">
                            <span class="card-title home">Text</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--
    <div class="divider"></div> --}}
    <div class="parallax-container">
        <div class="parallax">
            <img src="https://picsum.photos/1920/1080/">
        </div>
    </div>
    <section id="Contact">
        <div class="container">
            <div class="row center">
                <div class="col s12 m6">
                    <span>
                        <h2>Contact Us</h2>
                    </span>
                </div>
            </div>
            <div class="row center">
                <div class="col s12 m6">
                    <div class="card">map and details</div>
                </div>
                <div class="col s12 m6">

                    <div class="card" style="padding: 12px;">
                        Feel free to do business with us...
                        <br>
                        <form>
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="name" type="text" class="validate">
                                <label for="name">Your Name</label>
                            </div>
                            <div class="input-field">
                                <i class="material-icons prefix">mail</i>
                                <input id="email" type="email" class="validate">
                                <label for="email">Your email</label>
                            </div>
                            <div class="input-field">
                                <i class="material-icons prefix">short_text</i>
                                <input id="subject" type="text" class="validate">
                                <label for="subject">Subject</label>
                            </div>
                            <div class="input-field">
                                <i class="material-icons prefix">subject</i>
                                <textarea id="message" class="materialize-textarea"></textarea>
                                <label for="message">Message</label>
                            </div>
                            <a class="waves-effect waves-light btn-small orange">Send</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="page-footer teal">
        <div class="container">
            <div class="row">
                <div class="col s12 l6">
                    <h5 class="white-text">Footer Content</h5>
                    <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
                </div>
                <div class="col offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li>
                            <a class="grey-text text-lighten-3" href="#!">Link 1</a>
                        </li>
                        <li>
                            <a class="grey-text text-lighten-3" href="#!">Link 2</a>
                        </li>
                        <li>
                            <a class="grey-text text-lighten-3" href="#!">Link 3</a>
                        </li>
                        <li>
                            <a class="grey-text text-lighten-3" href="#!">Link 4</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2018 | All rights reserved
                <span class="right">Designed by &nbsp;
                    <a class="grey-text text-lighten-4 right" href="https://lewis-munyi.github.io">Lewis Munyi</a>
                </span>
            </div>
        </div>
    </footer>
    <!--Materialize and Jquery-->
    
    {{--  <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>  --}}
    <script type="text/javascript" src="js/main.js"></script>
</body>

</html>