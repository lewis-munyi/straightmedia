$(document).ready(function () {
    $('.scrollspy').scrollSpy();
    $('.carousel').carousel();
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
    });
    $('.sidenav').sidenav();
    // var navbar = $('.navbar');
    // var target = $('#' + navbar.attr("data-target"));
    // navbar.pushpin({
    //     top: target.offset().top,
    // });
    $('.materialboxed').materialbox();
    $('.parallax').parallax();
});
// Register Service worker
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register('/serviceworker.js').then(function (registration) {
            // console.log("Registration was successful");
        }, function (err) {
            // console.log("registration failed :(", err);
        });
    });
}